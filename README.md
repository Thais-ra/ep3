# Lojinha Deb

A Lojinha deb é um projeto que tem como objetivo arrecadar fundos para a realização da primeira MiniDebConf em Brasília.
Mas do que se trata?
É uma aplicação web de comercio eletrônico que atualmente está bem simples, abrangindo página inicial, página da loja, vizualização de produto, area de controle administrativo, cadastro, login e perfil de usuário.

## Funcionalidades

- Cadastro e login de usuário com a utilização da Gem Devise
- Painel do administrador com a Gem Rails_admin
- Restrição de acesso com a Gem Cancancan

# Como compilar

- Não é preciso se preocupar com a instalação de dependencias para ter acesso a toda aplicação. Você deverá ter somente o Docker e o Docker-compose instalado na sua maquina.

- Clone o repositório, e a partir da pasta rode os comandos:

```bash
	$sudo docker-compose up --build
```
Esse comando só será necessario na primeira vez que você rodar a aplicação no seu computador, depois é só usar o comando:

```bash
	$sudo docker-compose up -d
```
- Abra seu navegador favorito e acesse 'http://localhost:3000'

- Quando terminar de utilizar, não se esqueça de executar o comando:

```bash
	$sudo docker-compose down
```
# Estilização

- [Link](https://www.figma.com/file/MSEi2f3LVkIpq3JcpQkneR/Untitled?node-id=0%3A1) para protótipo da aplicação
- [Link](https://www.figma.com/file/8vImNQwI05yF9uVznH4Phw/LojinhaDeb-Colors) paleta de cores


