class CreateProdutos < ActiveRecord::Migration[6.0]
  def change
    create_table :produtos do |t|
      t.integer :codigo
      t.string :nome
      t.text :descricao
      t.references :categoria, null: false, foreign_key: true
      t.float :valor
      t.string :tamanho
      t.integer :status

      t.timestamps
    end
  end
end
