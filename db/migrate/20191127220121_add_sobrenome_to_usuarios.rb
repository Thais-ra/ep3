class AddSobrenomeToUsuarios < ActiveRecord::Migration[6.0]
  def change
    add_column :usuarios, :sobrenome, :string
  end
end
