class CreatePedidos < ActiveRecord::Migration[6.0]
  def change
    create_table :pedidos do |t|
      t.references :usuario, null: false, foreign_key: true
      t.integer :codigo
      t.date :data
      t.float :total

      t.timestamps
    end
  end
end
