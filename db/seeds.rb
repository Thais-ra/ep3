# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Usuario.create nome: 'José', sobrenome: 'bezerra', 
               tipo: :administrador, email: 'adm@teste.com', password: 123456

Usuario.create nome: 'Joana', sobrenome: 'bezerra', 
               tipo: :cliente, email: 'cliente@teste.com', password: 123456

Usuario.create nome: 'Thais', sobrenome: 'Rebouças', 
               tipo: :administrador, email: 'thaysreb@gmail.com', password: 123456
