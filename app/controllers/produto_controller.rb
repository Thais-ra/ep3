class ProdutoController < ApplicationController
    def prepara_sticker
        @produto = Produto.new
        @produto.nome = 'Sticker'
        @produto.descricao = 'Adesivo de computador branco com o simbolo do Debian. Medidas: 3 cm x 5cm'
        @produto.valor = 5.0
        @produto.save
    end
end
