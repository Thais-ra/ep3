Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  get 'paginas/index'
  get 'pagina_inicial/index'
  match 'loja' => 'loja#index', via: 'get'
  match 'encomenda_stickers' => 'produto#prepara_sticker', via: 'post'

  devise_for :usuarios
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'pagina_inicial#index'
end
