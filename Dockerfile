FROM ruby:2.5.1
COPY . /ep3
RUN curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh
RUN chmod +x nodesource_setup.sh
RUN ./nodesource_setup.sh

RUN apt-get update -qq && apt-get install -y nodejs postgresql-client

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y yarn

RUN apt-get install -qy dnsutils && dig gitlab.com

WORKDIR /ep3
COPY Gemfile /ep3/Gemfile
RUN gem install pg execjs
ENV BUNDLER_VERSION 2.0.2
RUN gem install bundler && bundle install
EXPOSE 3000
CMD ["rails", "server", "-b", "0.0.0.0"]

